# GetPublii Low Tech Theme

[![GPLv3 license](https://img.shields.io/badge/License-GPLv3-blue.svg)][![Maintenance](https://img.shields.io/badge/Maintained%3F-yes-green.svg)]


This theme is made for small blogs and showcase websites that would like to respect ecodesign and low-tech principles.

### What are the principles?

* Frugality instead of efficiency
* Only use the required feature
* Low-tech is accessible and private by nature

There are some tweaks, this theme follows a minimalist approach to reduce the energy cost of this theme and improve the accessibility.

Include minimal CSS, write with tachyons and few custom classes with a gulp workflow.

### Simplification

* No Google fonts, a font stack is more performant
* No gallery, so you can reduce the number of images and the media
* No AMP for privacy reasons and to avoid Google controls of Internet
* Remove Lazyloading, it's easier to limit the number of media, optimize the images or use SVG
* No icon for social media because they help to track users
* Support Fediverse social media (please use them instead of the GAFAM)

### Security recommendation

You can integrate these security options to increase the security of your website

* Cross scripting
* X-Frame-Options
* X-Content-Type-Options
* X-XSS-Protection

See [MDN doc](https://developer.mozilla.org/en-US/docs/Web/HTTP/Headers) and [Mozilla observatory](https://observatory.mozilla.org/)

## Theme development

Follow the principle of GetPublii.

CSS is based on [tachyons.css](https://tachyons.io/) and gulp tasks, but you can remove the PostCSS folder and work directly on the main.css into CSS folder.

CSS are divided in two folders
* PostCSS folder: use a gulp tasks to make the process easier.
* CSS folder: use to generate the final CSS

## GetPublii

GetPublii low tech theme :
This theme focuses on the minimum requirement to create an accessible and low tech website. This website aims to reduce the ecological footprint of a blog website.

More details: [GetPublii official website](https://getpublii.com/)

### Getting Started
You can learn more about GetPublii in the [User documentation](https://getpublii.com/docs/) or the [Developer documentation](https://getpublii.com/dev/).

## Where to host?

Everywhere, GetPublii generates a static website, any servers can host it.
You can even host it yourself with a simple raspberry pi.

## Feedback / Improvements

Everywhere, GetPublii generates a static website, any servers can host it.
You can even host it yourself with a simple raspberry pi.
