// Requis
var gulp = require('gulp');
var postcss = require('gulp-postcss');
var uncss = require('postcss-uncss');
var merge = require('postcss-import');
var cssnext = require('postcss-cssnext');
var rename = require('gulp-rename');

// Include plugins
var plugins = require('gulp-load-plugins')(); // tous les plugins de package.json

// Variables de chemins
var source = 'assets/postcss'; // dossier de travail
var destination = 'themes/assets/css'; // dossier à livrer

gulp.task('merge', function () {
  var comb = [
    merge
  ];
  return gulp.src('assets/postcss/tachyons.css')
    .pipe(postcss(comb))
    .pipe(rename('main.css'))
    .pipe(gulp.dest('assets/postcss/'));
});

gulp.task('css', function () {
  var tools = [
    cssnext,
    uncss({
      html: ['../../../output/**/*.html','../../../output/*.html'],
      ignore: ['.fade']
    })
  ];
  return gulp.src('assets/postcss/main.css')
    .pipe(plugins.cssbeautify({indent: '  '}))
    .pipe(postcss(tools))
    .pipe(gulp.dest('assets/css/'));
});

// Tâche "build"
gulp.task('build', gulp.series('merge', 'css'));
