/*
 * Custom function used to generate the output of the override.css file
 */

var generateOverride = function (params) {
    let output = '';

    /*****************
    PARAMS THEME COLOR
    *****************/

    if (params.textColor !== '#17181E') {
        output += `
        body,
        blockquote::before,
        pre > code {
          color: ${params.textColor};
        }`;
    }

    if (params.headingColor !== '#17181E' || params.fontH1Transform !== 'none') {
        output += `
        h1,
        h2,
        h3,
        h4,
        h5,
        h6 {
         color: ${params.headingColor};
         text-transform: ${params.fontH1Transform};
        }

        h1 > a,
        h2 > a,
        h3 > a,
        h4 > a,
        h5 > a,
        h6 > a {
         color: ${params.headingColor};
         text-transform: ${params.fontH1Transform};
        }

        hr::before {
          color: ${params.headingColor};

        }`;
    }

    if (params.linkColor !== '#D73A42') {
        output += `
        a,
        .invert:hover,
        .invert:active,
        .invert:focus {
               color: ${params.linkColor};
        }`;
    }

    if (params.linkHoverColor !== '#17181E') {
        output += `
        a:hover,
        a:active,
        a:focus {
          color: ${params.linkHoverColor};
        }

        .invert {
          color: ${params.linkHoverColor};
        }`;
    }

    if (params.selectionColor !== '#17181E') {
        output += `
        ::selection {
          background: ${params.selectionColor};
        };`
    }

    if (params.primaryColor !== '#D73A42') {
        output += `
        ::-moz-selection {
          background: ${params.primaryColor};
        }

        [type=text]:focus,
        [type=url]:focus,
        [type=tel]:focus,
        [type=number]:focus,
        [type=email]:focus,
        [type=search]:focus,
        textarea:focus,
        select:focus,
        select[multiple]:focus {
          border-color: ${params.primaryColor};
        }

        input[type=checkbox]:checked + label:before{
          background-image: url("data:image/svg+xml;charset=UTF-8,%3csvg xmlns='http://www.w3.org/2000/svg' viewBox='0 0 11 8'%3e%3cpolygon points='9.53 0 4.4 5.09 1.47 2.18 0 3.64 2.93 6.54 4.4 8 5.87 6.54 11 1.46 9.53 0' fill='${params.primaryColor.replace('#', '%23')}'/%3e%3c/svg%3e");
        }

        input[type=radio]:checked + label:before {
          background-image: url("data:image/svg+xml;charset=UTF-8,%3csvg xmlns='http://www.w3.org/2000/svg' viewBox='0 0 8 8'%3e%3ccircle cx='4' cy='4' r='4' fill='${params.primaryColor.replace('#', '%23')}'/%3e%3c/svg%3e");
        }

        code {
          color: ${params.primaryColor};
        }`;
    }

    /******************
    PARAMS HEADER
    ******************/

    if(params.backgroundTop !== '#17181E') {
        output += `
         #top {
          background: ${params.backgroundTop};
        }`;
    }

    if(params.backgroundMenu !== '#17181E') {
        output += `
         #menu {
          background: ${params.backgroundMenu};
        }`;
    }

    if (params.linkColorMenu !== '#FFFFFF') {
        output += `
        #menu ul li a,
        #menu ul li a span[aria-haspopup="true"] {
          color: ${params.linkColorMenu};
        }

        #menu ul li span {
          color: ${params.linkColorMenu};
        }`;
    }

    if (params.linkHoverColorMenu !== 'rgba(255, 255, 255, 0.7)') {
        output += `
        #menu ul li a:active,
        #menu ul li a:focus,
        #menu ul li a:hover,
        #menu ul li span[aria-haspopup="true"]:active,
        #menu ul li span[aria-haspopup="true"]:focus,
        #menu ul li span[aria-haspopup="true"]:hover   {
          color: ${params.linkHoverColorMenu};
        }

        #menu ul > li:hover > a,
        #menu ul > li:hover > span[aria-haspopup="true"] {
          color: ${params.linkHoverColorMenu};
        }`;
    }
    if(params.submenuBg !== '#17181E') {
        output += `
         .submenu  {
          background: ${params.submenuBg};
        }`;
    }

    if(params.submenuLinkColor !== 'rgba(255, 255, 255, 0.7)') {
        output += `
         .submenu li a,
         .submenu li span[aria-haspopup="true"] {
           color: ${params.submenuLinkColor} !important;
         }

         .submenu li span {
           color: ${params.submenuLinkColor} !important;
        }`;
    }

    if(params.submenuLinkHoverColor !== '#FFFFFF' || params.submenuLinkHoverBgMenu !== 'rgba(255, 255, 255, 0.05)') {
        output += `
         .submenu li a:active,
         .submenu li a:focus,
         .submenu li a:hover,
         .submenu li span[aria-haspopup="true"]:active,
         .submenu li span[aria-haspopup="true"]:focus,
         .submenu li span[aria-haspopup="true"]:hover {
           background: ${params.submenuLinkHoverBgMenu};
           color: ${params.submenuLinkHoverColor} !important;
         }

         .submenu li:hover > a,
         .submenu li:hover > span[aria-haspopup="true"] {
           color: ${params.submenuLinkHoverColor} !important;
        }`;
    }

    /******************
    PARAMS FOOTER
    ******************/

    if (params.footerBg !== '#17181E') {
        output += `
        #footer {
          background: ${params.footerBg};
        }`;
    }

    if (params.footerTextColor !== '#747577') {
        output += `
        #copyright {
          color: ${params.footerTextColor};
        }
        #copyright a {
          color: ${params.footerTextColor};
        }`;
    }

    if (params.footerLinkColor !== '#FFFFFF') {
        output += `
        .footer a {
          color: ${params.footerLinkColor};
        }`;
    }

    if (params.footerLinkHoverColor !== 'rgba(255, 255, 255, 0.7)') {
        output += `
        .footer a:hover {
          color: ${params.footerLinkHoverColor};
        }`;
    }

    return output;
}

module.exports = generateOverride;
